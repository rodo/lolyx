# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Routing functions
"""
import logging
import requests
from django.conf import settings


logger = logging.getLogger(__name__)

def readjson(url):
    """Read external JSON

    Params : url (string), url to fetch

    Return : string
    """
    try:
        uag = settings.USER_AGENT
    except:
        # TODO log an error
        uag = "python-request"

    headers = {'User-Agent': uag}

    try:
        data = requests.get(url,
                            timeout=settings.ROSARKS_TIMEOUT,
                            headers=headers)
        content = data.content
    except:
        content = "{}"

    return content
