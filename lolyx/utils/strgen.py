#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
 Copyright (C) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from uuid import uuid4


def email_random(suffix=''):
    """Return a random email

    based on random part and domain defined in settings
    """
    parts = str(uuid4()).split('-')
    email = "%s%s" % (parts[0], parts[1])

    return "%s%s@%s" % (email,
                        suffix,
                        settings.VMAIL_ALIAS_DOMAIN)

def resume_email_random():
    return "%s" % (email_random('4e2'))


def clean_urlfilter(ufilter):
    """Return ARRAY
    """
    toolstr = clean_urlfilter_str(ufilter)    

    tools = toolstr.split(',')

    if tools[0] == '':
        tools = []

    return tools


def clean_urlfilter_str(ufilter):
    """Remove commas from url
    """
    ufilter = ufilter.replace(',,',',')

    while ufilter.endswith(','):
        ufilter = ufilter[:-1]

    while ufilter.startswith(','):
        ufilter = ufilter[1:]        
    
    return ufilter
