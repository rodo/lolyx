# -*- coding: utf-8 -*-
"""
 Copyright (C) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.conf import settings
from TwitterAPI import TwitterAPI, TwitterOAuth, TwitterRestPager


def update_status(status):
    """Update twitter status

    status : (string) the status string to publish
    """
    api = TwitterAPI(settings.TWITTER_CONSUMER_KEY,
                     settings.TWITTER_CONSUMER_SECRET,
                     settings.TWITTER_ACCESS_TOKEN_KEY,
                     settings.TWITTER_ACCESS_TOKEN_SECRET)

    r = api.request('statuses/update', {'status': status})

    if r.status_code == 200:
        return 0
    else:
        return r.status_code
