# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('resume', '0002_auto_20150607_1153'),
    ]

    operations = [
        migrations.AddField(
            model_name='resumelog',
            name='reason',
            field=models.PositiveSmallIntegerField(default=0, choices=[(0, b'Ne cherche plus'), (1, b'Find by Lolix'), (2, b'Find another way'), (3, b'End of time (robot)')]),
        ),
    ]
