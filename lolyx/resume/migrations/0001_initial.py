# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.db.models.deletion
from django.conf import settings
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('llx', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='CompanyFollowed',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('company', models.ForeignKey(to='llx.Company')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Resume',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=300, null=True, verbose_name=b'Title', blank=True)),
                ('name', models.CharField(max_length=50)),
                ('firstname', models.CharField(max_length=50)),
                ('family', models.IntegerField(default=0, choices=[(0, b'Emploi'), (1, b'Stage'), (2, b'Mission')])),
                ('poste_desc', models.TextField(blank=True)),
                ('poste_desc_markup', models.PositiveSmallIntegerField(default=0, choices=[(0, b'text/plain'), (1, b'markdown')])),
                ('status', models.PositiveSmallIntegerField(default=0, choices=[(0, b'inactive'), (1, b'active')])),
                ('viewed', models.IntegerField(default=0)),
                ('email', models.EmailField(max_length=300)),
                ('email_verified', models.BooleanField(default=False)),
                ('email_internal', models.EmailField(unique=True, max_length=300)),
                ('date_published', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('date_updated', models.DateTimeField(auto_now=True)),
                ('date_expired', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.PROTECT)),
            ],
        ),
        migrations.CreateModel(
            name='ResumeExperience',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_deb', models.DateField()),
                ('date_fin', models.DateField()),
                ('title', models.CharField(max_length=100)),
                ('desc', models.TextField(blank=True)),
                ('desc_markup', models.PositiveSmallIntegerField(default=0, choices=[(0, b'text/plain'), (1, b'markdown')])),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeLang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.SmallIntegerField(default=3)),
                ('tms', models.DateTimeField(auto_now_add=True)),
                ('lang', models.ForeignKey(to='llx.Lang')),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('status', models.PositiveSmallIntegerField(default=0, choices=[(0, b'inactive'), (1, b'active')])),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeOnline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tools', django.contrib.postgres.fields.ArrayField(base_field=models.CharField(max_length=30), size=None)),
                ('family', models.IntegerField(default=0, choices=[(0, b'Emploi'), (1, b'Stage'), (2, b'Mission')])),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeRegion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pref', models.PositiveSmallIntegerField(default=0)),
                ('region', models.ForeignKey(to='llx.Region')),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('date', models.DateField(auto_now_add=True)),
                ('resume', models.ForeignKey(to='resume.Resume')),
            ],
        ),
        migrations.CreateModel(
            name='ResumeTool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.PositiveSmallIntegerField(default=3)),
                ('contrib', models.PositiveSmallIntegerField(default=0)),
                ('experience', models.PositiveSmallIntegerField(default=0)),
                ('resume', models.ForeignKey(to='resume.Resume')),
                ('tool', models.ForeignKey(to='llx.Tool', on_delete=django.db.models.deletion.PROTECT)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='resumetool',
            unique_together=set([('resume', 'tool')]),
        ),
        migrations.AlterUniqueTogether(
            name='resumelang',
            unique_together=set([('resume', 'lang')]),
        ),
    ]
