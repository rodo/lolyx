#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Remove from online resumes expired

A resume is published for a fixed number of days defined in settings,
after this period if the resume is still online it will be unpublished
"""
import logging
from datetime import datetime
from django.core.mail import send_mail
from django.conf import settings
from django.core.management.base import BaseCommand
from lolyx.resume.models import Resume

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Publish a resume'

    def handle(self, *args, **options):
        """
        Handle the command
        """
        resume = Resume.objects.filter(resume_id=173)

        resume.dopublish()
