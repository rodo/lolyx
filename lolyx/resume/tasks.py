# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Tasks related to resume
"""
import logging
from celery.task import task
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.core.mail import send_mail
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from lolyx.utils.OsmTools import OsrmAPI


logger = logging.getLogger(__name__)


@task()
def compute_trips(resume):
    """Compute trips
    """
    rt = OsrmAPI()
    rt.host = settings.OSRMAPI_HOST

    for moder in moderators:
        logger.debug("send email to [%s]" % (moder.email))

        subject = render_to_string("job/emails/job_moderation_subject.txt", {'job': job})
        msg = render_to_string("job/emails/job_moderation_body.txt", {'job': job,
                                                                      'email': moder.email})

        send_mail(subject.strip(),
                  msg,
                  settings.EMAIL_FROM, [moder.email])


@task()
def mail_resume_is_online(job):
    """Send an email to inform author that is resume is online

    A mail is sent to te author to inform her/him thit his/her job is online
    """

    logger.debug("(%s) send email to [%s]" % ('mail_job_is_online',
                                              job.contact_email))

    tmpl_subj = "job/emails/job_online_subject.txt"
    tmpl_body = "job/emails/job_online_body.txt"

    subject = render_to_string(tmpl_subj, {'job': job})
    msg = render_to_string(tmpl_body, {'job': job})

    if len(job.contact_email) > 0:
        send_mail(subject.strip(),
                  msg,
                  settings.EMAIL_FROM, [job.contact_email])
