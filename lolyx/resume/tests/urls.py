# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test all public and private urls
"""
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import Client
from lolyx.resume.models import Resume
from lolyx.llx.models import UserExtra


class UrlsTests(TestCase):  # pylint: disable-msg=R0904
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        Resume.objects.all().delete()
        UserExtra.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_view(self):
        """Basic detailview
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=1)

        client = Client()
        response = client.get('/cv/{}/'.format(resume.id))

        self.assertContains(response, resume.title, status_code=200)

    def test_view_status(self):
        """Basic detailview

        Raise an error 404 when tying to access a Resume not published
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=0)

        client = Client()
        response = client.get('/cv/{}/'.format(resume.id))
        self.assertEqual(response.status_code, 404)

    def test_view_markdown(self):
        """
        The view with poste description in markdown
        """
        poste_desc = "lorem ipsum\r\n\r\n * ipsum\r\n * lorem"

        resume = Resume.objects.create(title='Senior admin',
                                       status=1,
                                       poste_desc=poste_desc,
                                       poste_desc_markup=1,
                                       user=self.user)

        client = Client()
        response = client.get('/cv/{}/'.format(resume.id))

        self.assertContains(response, "lorem", status_code=200)

    def test_view_rest(self):
        """
        The view with poste description in rest
        """
        poste_desc = "lorem ipsum\r\n\r\n * ipsum\r\n * lorem"

        resume = Resume.objects.create(title='Senior admin',
                                       poste_desc=poste_desc,
                                       poste_desc_markup=2,
                                       user=self.user,
                                       status=1)

        client = Client()
        response = client.get('/cv/{}/'.format(resume.id))

        self.assertContains(response, "lorem", status_code=200)

    def test_view_extra(self):
        """
        The view with extra contents
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=1)

        extra = '{"Github": {"public_repos": 27, "type": "repo"}}'

        UserExtra.objects.create(user=self.user,
                                 extra=extra)

        client = Client()
        response = client.get('/cv/{}/'.format(resume.id))

        self.assertContains(response, "Github", status_code=200)
        self.assertContains(response, "27", status_code=200)

    def test_edit(self):
        """Call the edit view with regular user
        """
        user = User.objects.create_user('edit_regular',
                                        'admin_search@bar.com',
                                        'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=user,
                                       status=1)

        client = Client()
        client.login(username='edit_regular', password='admintest')
        response = client.get('/cv/{}/edit/'.format(resume.id))

        self.assertContains(response, resume.title, status_code=200)

    def test_edit_unauth(self):
        """Call the edit view with another user

        The user doesn't own the resume
        """
        User.objects.create_user('edit_unauth',
                                 'admin_search@bar.com',
                                 'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=1)

        client = Client()
        auth = client.login(username='edit_unauth', password='admintest')
        response = client.get('/cv/{}/edit/'.format(resume.id))

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)

    def test_new_resume_notlogged(self):
        """
        New resume when not log redirect
        """
        client = Client()
        response = client.get('/cv/new/')

        self.assertEqual(response.status_code, 302)

    def test_new_resume_logged(self):
        """
        New resume when logged
        """
        client = Client()
        auth = client.login(username='foobar', password='admintest')
        response = client.get('/cv/new/')

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)

    def test_resume_newpost(self):
        """
        The form is valid
        """
        token = '88c25fbe6ed4871bf9e8e83820a4e001'
        datas = {'title': 'foobar',
                 'csrfmiddlewaretoken': token}

        client = Client()
        client.login(username='foobar', password='admintest')

        resp = client.post('/cv/new/', datas)

        self.assertEqual(resp.status_code, 200)

    def test_list_public(self):
        """The public list

        - must show Resume with status == 1 only
        """
        Resume.objects.create(title='SeniorFoobar',
                              user=self.user,
                              status=1)

        Resume.objects.create(title='BarfooHidden',
                              user=self.user,
                              status=0)

        extra = '{"Github": {"public_repos": 27, "type": "repo"}}'

        UserExtra.objects.create(user=self.user,
                                 extra=extra)

        client = Client()
        response = client.get('/cv/')

        self.assertContains(response, 'SeniorFoobar', status_code=200)
        self.assertNotContains(response, 'BarfooHidden', status_code=200)
