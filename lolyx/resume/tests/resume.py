# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Resume object

"""
from datetime import datetime
from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase
from django.db import IntegrityError
from lolyx.resume.models import Resume, ResumeTool, ResumeLog
from lolyx.llx.models import Tool


class ResumeTests(TestCase):
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        ResumeLog.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_create(self):
        """
        Create a simple resume
        """
        title = 'Senior admin'
        resume = Resume.objects.create(title=title,
                                       user=self.user)

        self.assertTrue(resume.id > 0)
        self.assertEqual(resume.status, 0)
        self.assertEqual(str(resume), title)
        self.assertEqual(unicode(resume), title)

    def test_resumelog(self):
        """
        When a resume is create a resumelog is created too
        """
        title = 'Senior admin'
        Resume.objects.create(title=title, user=self.user)

        rsc = ResumeLog.objects.all().count()

        self.assertEqual(rsc, 1)

    def test_absolute_url(self):
        """
        Create a simple resume
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user)

        url = "/cv/{}/".format(resume.id)

        self.assertEqual(resume.get_absolute_url(), url)

    def test_create_tool(self):
        """
        Create a resume with a tool
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user)

        tool = Tool.objects.create(name='foo', slug='foo')

        restool = ResumeTool.objects.create(resume=resume,
                                            tool=tool)

        self.assertTrue(restool.id > 0)

    def test_create_tools(self):
        """
        Create a resume with more than one tool
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user)

        tool1 = Tool.objects.create(name='foo1', slug='foo1')
        tool2 = Tool.objects.create(name='foo2', slug='foo2')

        restool1 = ResumeTool.objects.create(resume=resume,
                                             tool=tool1)

        restool2 = ResumeTool.objects.create(resume=resume,
                                             tool=tool2)

        self.assertTrue(restool1.id > 0)
        self.assertTrue(restool2.id > 0)

    def test_create_tool_unique(self):
        """
        Check unicity of resume/tool couple
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user)

        tool1 = Tool.objects.create(name='foo1', slug='foo1')

        ResumeTool.objects.create(resume=resume,
                                  tool=tool1)

        with self.assertRaises(IntegrityError):
            ResumeTool.objects.create(resume=resume, tool=tool1)

    def test_publish_offline(self):
        """Publish a resume

        - resume is not already published online
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=settings.RESUME_STATUS_EDITION)

        date_published = resume.date_published
        date_expired = resume.date_expired
        resume.publish()

        nres = Resume.objects.get(pk=resume.id)

        self.assertEqual(nres.status, settings.RESUME_STATUS_PUBLISHED)
        self.assertNotEqual(date_published, nres.date_published)
        self.assertNotEqual(date_expired, nres.date_expired)

    def test_publish_online(self):
        """Publish a resume

        - resume is already published
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=settings.RESUME_STATUS_PUBLISHED)

        date_published = resume.date_published
        date_expired = resume.date_expired
        resume.publish()

        nres = Resume.objects.get(pk=resume.id)

        self.assertEqual(nres.status, settings.RESUME_STATUS_PUBLISHED)
        self.assertEqual(date_published, nres.date_published)
        self.assertEqual(date_expired, nres.date_expired)

    def test_unpublish_online(self):
        """Unpublish a resume

        - resume is online
        """
        # the date expired is in past
        date_expired = datetime(2014, 1, 1, 12, 34, 17)

        resume = Resume.objects.create(
            title='Senior admin',
            user=self.user,
            status=settings.RESUME_STATUS_PUBLISHED,
            date_expired=date_expired)

        resume.unpublish()  # action

        nres = Resume.objects.get(pk=resume.id)

        self.assertEqual(nres.status, settings.RESUME_STATUS_EDITION)
        self.assertNotEqual(date_expired, nres.date_expired)

    def test_unpublish_not_online(self):
        """Unpublish a resume

        - resume is not online
        """
        # the date expired is in past
        date_expired = datetime(2014, 1, 1, 12, 34, 17)

        resume = Resume.objects.create(
            title='Senior admin',
            user=self.user,
            status=settings.RESUME_STATUS_EDITION,
            date_expired=date_expired)

        resume.unpublish()  # action

        nres = Resume.objects.get(pk=resume.id)

        self.assertEqual(nres.status, settings.RESUME_STATUS_EDITION)
        self.assertEqual(date_expired, nres.date_expired)
