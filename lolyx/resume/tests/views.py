# -*- coding: utf-8 -*-
#
# Copyright (c) 2013, 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test views
"""
import json
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import RequestFactory
from lolyx.resume.models import Resume
from lolyx.resume.models import ResumeStat
from lolyx.llx.models import UserExtra
from lolyx.resume import views


class ViewsTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        ResumeStat.objects.all().delete()
        Resume.objects.all().delete()
        UserExtra.objects.all().delete()
        User.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_resume_store_stat(self):
        """Resume public detail view

        Whean accessing this view we log a line in table ResumeStat
        """
        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=1)

        request = RequestFactory().get('/fake-path')
        view = views.ResumeView.as_view()

        view(request, pk=resume.id)  # action

        nbs = ResumeStat.objects.all().count()
        self.assertEqual(nbs, 1)

    def test_context(self):
        """Resume public detail view

        Whean accessing this view we log a line in table ResumeStat
        """
        myuser = User.objects.create_user('myuser',
                                          'admin_search@bar.com',
                                          'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=myuser,
                                       status=1)

        UserExtra.objects.create(
            user=myuser,
            extra=json.dumps(
                {"Bitbucket": {"url": "https://bitbucket.org/rodolphe",
                               "public_repos": 4,
                               "type": "repo",
                               "api_status": 200},
                 "GitHub": {"url": "https://github.com/rodo/",
                            "public_repos": 28,
                            "created_at": "2009-05-18T12:07:44Z",
                            "type": "repo", "api_status": 200},
                 "Linuxfr": {"url": "http://linuxfr.org/users/rodolphe",
                             "status": 200,
                             "type": "userpage"}}))

        view = views.ResumeView()
        setattr(view, 'object', resume)
        context = view.get_context_data()

        self.assertEqual(len(context), 4)
        self.assertEqual(len(context['repo']), 3)
        self.assertEqual(len(context['tools']), 0)
        self.assertEqual(len(context['userpage']), 1)

    def test_publish_owner(self):
        """Publish a resume
        """
        user = User.objects.create_user('user_publish',
                                        'admin_search@bar.com',
                                        'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=user,
                                       status=settings.JOB_STATUS_EDITION)

        request = RequestFactory().get('/fake-path')
        request.user = user

        response = views.publish(request, resume.id)  # action

        self.assertEqual(response.status_code, 302)

    def test_publish_notowner(self):
        """Publish a resume
        """
        user = User.objects.create_user('user_publish',
                                        'admin_search@bar.com',
                                        'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=settings.JOB_STATUS_EDITION)

        request = RequestFactory().get('/fake-path')
        request.user = user

        with self.assertRaises(PermissionDenied):
            views.publish(request, resume.id)  # action

    def test_unpublish_owner(self):
        """Unpublish a resume

        - user : is the owner of the resume
        - resume : status
        """
        user = User.objects.create_user('user_publish',
                                        'admin_search@bar.com',
                                        'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=user,
                                       status=settings.JOB_STATUS_PUBLISHED)

        request = RequestFactory().get('/fake-path')
        request.user = user

        response = views.unpublish(request, resume.id)  # action

        self.assertEqual(response.status_code, 302)

    def test_unpublish_notowner(self):
        """Unpublish a resume

        - user : is NOT the owner of the resume
        - resume : status
        """
        user = User.objects.create_user('user_publish',
                                        'admin_search@bar.com',
                                        'admintest')

        resume = Resume.objects.create(title='Senior admin',
                                       user=self.user,
                                       status=settings.JOB_STATUS_PUBLISHED)

        request = RequestFactory().get('/fake-path')
        request.user = user

        with self.assertRaises(PermissionDenied):
            views.unpublish(request, resume.id)  # action

    def test_resumenew(self):
        """New resume GET

        Check initial values
        """
        last_name = "ohY3rahN"
        first_name = "eil6oj0E"

        user = User.objects.create_user('notowner',
                                        'admin_search@bar.com',
                                        'admintest')
        user.last_name = last_name
        user.first_name = first_name
        user.save()

        request = RequestFactory().get('/fake-path')
        request.user = user

        view = views.ResumeNew()
        setattr(view, 'object', None)
        setattr(view, 'request', request)
        initial = view.get_initial()

        self.assertEqual(initial['user'], user.id)
        self.assertEqual(initial['firstname'], first_name)
        self.assertEqual(initial['name'], last_name)
