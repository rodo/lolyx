from lolyx.resume.tests.urls import UrlsTests
from lolyx.resume.tests.resume import ResumeTests
from lolyx.resume.tests.resumeexperience import ResumeExperienceTests
from lolyx.resume.tests.views import ViewsTests
from lolyx.resume.tests.command import CommandTests
