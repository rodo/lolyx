/*
 * Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function switchTool(obj, id)
{
    if ($(obj).hasClass("btn-primary")) {
        delTool(obj, id);
    } else {
        addTool(obj, id);
    }
}


function addTool(obj, id) {

	var urladd = 'addtool/' + id;

	$.get(urladd,
		  function (data) {
              $(obj).removeClass('btn-default').addClass('btn-primary');
		  }, 'json');    
}

function delTool(obj, id) {

	var urladd = 'deltool/' + id;


    	$.get(urladd,
		  function (data) {
              $(obj).removeClass('btn-primary').addClass('btn-default');
		  }, 'json');    


}
