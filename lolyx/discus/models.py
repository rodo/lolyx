# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for resume
"""
from django.db import models
from django.contrib.auth.models import User


class Discus(models.Model):
    """
    A discussion that contains messages
    """
    user_from = models.ForeignKey(User,
                                  related_name='disuserfrom',
                                  on_delete=models.PROTECT)

    user_to = models.ForeignKey(User,
                                related_name='disuserto',
                                on_delete=models.PROTECT)

    opened = models.DateTimeField(auto_now=True)

    status = models.IntegerField(default=0,
                                 choices=((0, 'En cours'),
                                          (1, 'Archivée')))


class Message(models.Model):
    """
    A discussion that contains messages
    """
    discus = models.ForeignKey(Discus,
                               on_delete=models.PROTECT)

    body = models.CharField(max_length=300,
                            verbose_name='Title',
                            blank=True,
                            null=True)

    sent_on = models.DateTimeField(auto_now=True)
    unread = models.BooleanField(default=True)

    foo = models.DecimalField(decimal_places=2, max_digits=2)

    class Meta:
        index_together = (('id', 'sent_on'),)
