# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('discus', '0002_message_unread'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='message',
            index_together=set([('id', 'sent_on')]),
        ),
    ]
