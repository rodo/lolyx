# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Discus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('opened', models.DateTimeField(auto_now=True)),
                ('status', models.IntegerField(default=0, choices=[(0, b'En cours'), (1, b'Archiv\xc3\xa9e')])),
                ('user_from', models.ForeignKey(related_name='disuserfrom', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('user_to', models.ForeignKey(related_name='disuserto', on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.CharField(max_length=300, null=True, verbose_name=b'Title', blank=True)),
                ('sent_on', models.DateTimeField(auto_now=True)),
                ('discus', models.ForeignKey(to='discus.Discus', on_delete=django.db.models.deletion.PROTECT)),
            ],
        ),
    ]
