# -*- coding: utf-8 -*-
#
# Copyright (c) 2014-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The discus views
"""
import logging
import json
from django.conf import settings
from django.core import serializers
from django.core.urlresolvers import reverse_lazy
from django.core.urlresolvers import reverse
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseForbidden, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import ListView, DetailView, DeleteView, RedirectView
from lolyx.discus.models import Message
from lolyx.utils.view_mixins import ProtectedMixin

logger = logging.getLogger(__name__)


class MessageListView(ProtectedMixin, ListView):
    """

    ProtectedMixin: login is required
    """
    model = Message
    template_name = 'discus/message_list.html'

    def get_queryset(self):
        return Message.objects.filter(discus__user_to=self.request.user).order_by('-sent_on')


class MessageView(ProtectedMixin, ListView):
    """

    ProtectedMixin: login is required
    """
    model = Message
    template_name = 'discus/message_detail.html'

    def get_queryset(self):
        return Message.objects.filter(discus__user_to=self.request.user).order_by('-sent_on')
