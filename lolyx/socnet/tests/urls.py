# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test all public and private urls
"""
import json
from django.test import TestCase
from django.test import Client
from lolyx.socnet.models import Network


class UrlsTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        Network.objects.all().delete()

    def test_networks(self):
        """ /socnet/networks/

        Return json
        """
        Network.objects.create(name='Indus1')
        Network.objects.create(name='Indus2', enable=False)

        url = '/socnet/networks/'

        client = Client()
        response = client.get(url)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'application/json')
        self.assertEqual(1, len(data))
