# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Network',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('enable', models.BooleanField(default=True)),
                ('category', models.PositiveSmallIntegerField(default=0)),
                ('url', models.CharField(max_length=300)),
                ('homepage', models.URLField(max_length=300, null=True, blank=True)),
            ],
        ),
    ]
