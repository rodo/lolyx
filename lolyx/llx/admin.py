# -*- coding: utf-8 -*-
#
# Copyright (c) 2013-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib import admin
from lolyx.llx.models import (Company, Contract, Faq,
                              Effectif, Tool, Sector,
                              Motd,
                              Region, Poste,
                              Typent, UserPoint, UserProfile)


class CompanyAdmin(admin.ModelAdmin):
    """Custom Admin for Company
    """
    list_display = ('name', 'slug')
    list_filter = ('adh_april',)


class ContractAdmin(admin.ModelAdmin):
    """Custom Admin for Contract
    """
    list_display = ('code',)


class EffectifAdmin(admin.ModelAdmin):
    """Custom Admin for EFFECTIF
    """
    list_display = ('name',)


class FaqAdmin(admin.ModelAdmin):
    """Custom Admin for FAQ
    """
    list_display = ('question',)


class MotdAdmin(admin.ModelAdmin):
    """Custom Admin for MOTD
    """
    list_display = ('title', 'active', 'home')


class ToolAdmin(admin.ModelAdmin):
    """Custom Admin for Tool
    """
    list_display = ('name', 'url', 'odds')


class SectorAdmin(admin.ModelAdmin):
    """Custom Admin for Sector
    """
    list_display = ('name',)


class RegionAdmin(admin.ModelAdmin):
    """Custom Admin for Region
    """
    list_display = ('name',)


class TypentAdmin(admin.ModelAdmin):
    """Custom Admin for Typent
    """
    list_display = ('name',)


class UserPointAdmin(admin.ModelAdmin):
    """Custom Admin for UserPoint
    """
    list_display = ('user', 'active', 'points')
    search_fields = ('user', )


class UserProfileAdmin(admin.ModelAdmin):
    """Custom Admin for UserProfile
    """
    list_display = ('user',)
    search_fields = ('user', )


admin.site.register(Company, CompanyAdmin)
admin.site.register(Contract, ContractAdmin)
admin.site.register(Effectif, EffectifAdmin)
admin.site.register(Faq, FaqAdmin)
admin.site.register(Motd, MotdAdmin)
admin.site.register(Poste)
admin.site.register(Region, RegionAdmin)
admin.site.register(Sector, SectorAdmin)
admin.site.register(Tool, ToolAdmin)
admin.site.register(Typent, TypentAdmin)
admin.site.register(UserPoint, UserPointAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
