/*
 * Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 * First version with jobs per region
 *
 */
var geojson;
var families = ["emploi", "stage", "mission"];
var stats = [];

function maps() {
    mapi(0);
    mapi(1);
    mapi(2);
}

function mapi(i) {

    var url = "/offres/stats/"+families[i]+"/region.json";    
    $.getJSON(url, 
	      function( datas ) { 
		  var regions = [];
		  $.each(datas, function (data) {
			     regions[String(this.insee)] = this.offres;
			     
			 });
		  stats[i] = regions;
		  map(families[i], i);
	      });        
}

function map(familyname, family) {

    var div = familyname + "map";

    var map = L.map(div).setView([46.0, 2.0], 5);
    var osmfr = L.tileLayer(map_main_layers[0],
			    {attribution: map_main_layers[1]}).addTo(map);

    if (family == 0) {
	geojson = L.geoJson(statesData, {
				style: style_emploi,
				onEachFeature: onEachFeature
			    }).addTo(map);
    }
    if (family == 1) {
	geojson = L.geoJson(statesData, {
				style: style_stage,
				onEachFeature: onEachFeature
			    }).addTo(map);
    }
    if (family == 2) {
	geojson = L.geoJson(statesData, {
				style: style_mission,
				onEachFeature: onEachFeature
			    }).addTo(map);
    }

}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
		 click: zoomToFeature
	     });
}

// get color depending on population density value
function getColor(nom, family) {

    var d = getNbOffre(nom, family);

    var colors = mapcolors(family);

    return d > 50  ? colors[6] :
	d > 20  ? color[5] :
	d > 10  ? colors[4] :
	d > 50   ? colors[3] :
	d > 2   ? colors[2] :
	d > 0   ? colors[1] :
        colors[0];
}

function getNbOffre(nom, family) {   
    return stats[family][nom];
}

function style_emploi(feature) {

    var d = getNbOffre(feature.properties.code_insee, 0);
    var color = getColor(feature.properties.code_insee, 0);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};
    
    if (d ==0) {
	style = { weight: 1,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}

function style_stage(feature) {
    var d = getNbOffre(feature.properties.code_insee, 1);
    var color = getColor(feature.properties.code_insee, 1);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};
    
    if (d ==0) {
	style = { weight: 2,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}

function style_mission(feature) {
    var d = getNbOffre(feature.properties.code_insee, 2);
    var color = getColor(feature.properties.code_insee, 2);

    var style = { weight: 1,
		  opacity: 1,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0.8,
		  fillColor: color
		};
    
    if (d ==0) {
	style = { weight: 1,
		  opacity: 0,
		  color: 'black',
		  dashArray: '3',
		  fillOpacity: 0
		};
    }

    return style;
}
