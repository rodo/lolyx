/*
 * Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
function rosarks(lon, lat) {

    var div = "jobrosarks";
    var precision = 1000;
    var limit = 3;
    var url = "/rosarks/{{amenity}}/{{lon}}/{{lat}}/";

    $.getJSON(Mustache.render(url, {'amenity': 'bicycle_rental',
				    'lon': lon,
				    'lat': lat}),
	      {'limit': limit,
	       'precision': precision},
	      parse_bicycle_rental);

    $.getJSON(Mustache.render(url, {'amenity': 'tramway_station',
				    'lon': lon,
				    'lat': lat}),
	     {'limit': limit,
	      'precision': precision},
	      parse_tramway);

    $.getJSON(Mustache.render(url, {'amenity': 'subway_station',
				    'lon': lon,
				    'lat': lat}),
	      {'limit': limit,
	       'precision': precision},
	      parse_subway);

    $.getJSON(Mustache.render(url, {'amenity': 'bus_stop',
				    'lon': lon,
				    'lat': lat}),
	      {'limit': limit,
	       'precision': precision},
	      parse_busstop);
}

function parse_bicycle_rental(data) {
    var item = '<li id="osmid_{{osmid}}" onclick="mapgoto({{lon}},{{lat}});">{{name}}</li>';
    var amenities = data['datas'];
    var items = [];
    $.each( amenities,
	    function( key, val ) {

		items.push(Mustache.render(item, {'osmid': val.osmid,
						  'name': val.name,
						  'lon': val.lon,
						  'lat': val.lat}));
		viaroute(val.osmid, data['center'], [val.lon, val.lat]);
	    });

    if (items.length > 0) {
	var txtamenity = "<ul>" + items.join("") + "</ul>";
	$('#jobrosarks').append("Location vélo" + txtamenity);
    }
}


function parse_tramway(data) {
    var line = "<span class='badge badge-subway' style='background-color: {{colour}};'>{{ref}}</span>";
    var amenities = data['datas'];
    var items = [];
    $.each( amenities,
	    function( key, val ) {
		var item = "<li>" + val.name;
		$.each( val['tramway_lines'],
			function( key, val ) {
			    item = item + Mustache.render(line, val);
			});
		item = item + "</li>";
		items.push( item );
		viaroute(val.osmid, data['center'], [val.lon, val.lat]);
	    });

    if (items.length > 0) {
	var txtamenity = "<ul>" + items.join("") + "</ul>";
	$('#jobrosarks').append("Tramway" + txtamenity);
    }
}


function parse_subway(data) {
    var line = "<span class='badge badge-subway' style='background-color: {{colour}};'>{{ref}}</span>";
    var amenities = data['datas'];
    var items = [];
    $.each( amenities,
	    function( key, val ) {
		var item = "<li id='osmid_" + val.osmid +"'>" + val.name;
		$.each( val['subway_lines'],
			function( key, val ) {
			    item = item + Mustache.render(line, val);
			});
		item = item + "</li>";
		items.push( item );
		viaroute(val.osmid, data['center'], [val.lon, val.lat]);
	    });

    if (items.length > 0) {
	var txtamenity = "<ul>" + items.join("") + "</ul>";
	$('#jobrosarks').append("<p>Métro</p>" + txtamenity);
    }
}

function parse_busstop(data) {
    var line = "<span class='badge badge-bus' style='background-color: {{colour}};'>{{ref}}</span>";
    var amenities = data['datas'];
    var items = [];
    $.each( amenities,
	    function( key, val ) {
		var item = "<li id='osmid_" + val.osmid +"'>" + val.name;
		$.each( val['bus_lines'],
			function( key, val ) {
			    item = item + Mustache.render(line, val);
			});
		item = item + "</li>";
		items.push( item );
		viaroute(val.osmid, data['center'], [val.lon, val.lat]);
	    });

    if (items.length > 0) {
	var txtamenity = "<ul>" + items.join("") + "</ul>";
	$('#jobrosarks').append("<p>Bus</p>" + txtamenity);
    }
}


function viaroute(osmid, from, to) {
    /*
     * Call OSRM
     */
    var url = "/viaroute/?from={{from_lat}},{{from_lon}}&to={{to_lat}},{{to_lon}}";

    $.getJSON(Mustache.render(url, {'from_lon': from[0],
				    'from_lat': from[1],
				    'to_lon': to[0],
				    'to_lat': to[1]}),
	      {},
	      function (data) {
		  var dist = data['route_summary']['total_distance'];
		  if (dist > 0) {
		      $("#osmid_"+osmid).append(', ' + dist + ' m');
		  }
	      });

}

function mapgoto(lon, lat) {
    map.panTo(new L.LatLng(lat, lon));
}

