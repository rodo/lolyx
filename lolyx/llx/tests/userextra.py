# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Invitation object

"""
from django.contrib.auth.models import User
from django.test import TestCase
from lolyx.llx.models import UserExtra


class UserExtraTests(TestCase):  # pylint: disable-msg=R0904
    """
    The main tests
    """
    def setUp(self):
        """
        set up the tests
        """
        self.user1 = User.objects.create_user('foobar',
                                              'admin_search@foo.com',
                                              'adminfoo')

    def test_create(self):
        """
        Create a simple invitation
        """
        uext = UserExtra.objects.create(user=self.user1,
                                        extra='lorem ipsum')

        self.assertTrue(uext.id > 0)
