# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Invitation object

"""
from django.contrib.auth.models import User
from django.test import TestCase
from lolyx.llx.models import UserPoint


class UserPointTests(TestCase):  # pylint: disable-msg=R0904
    """
    The main tests
    """

    def test_userpoint_defaults(self):
        """
        Defaults values for UserPoint object
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        upt = UserPoint.objects.get(user=user)

        self.assertEqual(upt.points, 0)

    def test_removepoint(self):
        """Remove points
        """
        user = User.objects.create_user('foobar',
                                        'admin_search@foo.com',
                                        'adminfoo')

        upt = UserPoint.objects.get(user=user)
        upt.points = 500
        upt.save()

        upt.remove_points(100)

        upt = UserPoint.objects.get(user=user)

        self.assertEqual(upt.points, 400)
