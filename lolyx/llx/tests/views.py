# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test views in app llx
"""
from django.contrib.auth.models import User
from django.test import TestCase
from django.test import RequestFactory
from lolyx.llx.models import UserProfile
from lolyx.llx import views


class ViewsTests(TestCase):
    """
    Tests regarding the object Views
    """
    def setUp(self):
        """Set up the tests
        """
        self.user = User.objects.create_user('test_company',
                                             'admin_search@bar.com',
                                             'admintest')

    def test_profile_unknown(self):
        """Profile not yet choosen
        """
        user = User.objects.create_user('foobar_tpu',
                                        'admin_search@bar.com',
                                        'admintest')

        request = RequestFactory().get('/fake-path')
        request.user = user
        # Call the view
        response = views.profile(request)

        self.assertContains(response, 'llx/profile.html', status_code=200)

    def test_profile_unknown_badchoice(self):
        """Profile not yet choosen

        The user set a bad value
        """
        user = User.objects.create_user('foobar_tpu',
                                        'admin_search@bar.com',
                                        'admintest')

        # Set a bad status
        request = RequestFactory().get('/fake-path?set=4')
        request.user = user
        # Call the view
        response = views.profile(request)

        self.assertContains(response, 'llx/profile.html', status_code=200)

    def test_profile_resume(self):
        """Profile for candidate
        """
        user = User.objects.create_user('foobar_tpr',
                                        'admin_search@bar.com',
                                        'admintest')

        # define the profile to candidate
        prof = UserProfile.objects.get(user=user)
        prof.status = 1
        prof.save()

        request = RequestFactory().get('/fake-path')
        request.user = user
        response = views.profile(request)
        # Run.
        self.assertContains(response, 'resume/profile.html', status_code=200)

    def test_profile_recruiter(self):
        """Profile for recruiters
        """
        user = User.objects.create_user('foobar_tpr',
                                        'admin_search@bar.com',
                                        'admintest')
        # define the profile to recruiter
        prof = UserProfile.objects.get(user=user)
        prof.status = 2
        prof.save()

        request = RequestFactory().get('/fake-path')
        request.user = user
        response = views.profile(request)
        # Run.
        self.assertContains(response, 'job/profile.html', status_code=200)
