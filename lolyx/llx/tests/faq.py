# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for Frequently Ask Question
"""
from django.test import TestCase
from lolyx.llx.models import Faq


class FaqTests(TestCase):
    """
    The main tests
    """

    def test_create(self):
        """
        Create a simple faq entry
        """
        faq = Faq.objects.create(question='What is the life ?',
                                 answer='42')

        self.assertTrue(faq.id > 0)
        self.assertEqual(faq.score, 0)
        self.assertEqual(str(faq), 'What is the life ?')
        self.assertEqual(unicode(faq), 'What is the life ?')
