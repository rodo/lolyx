# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# Forms in llx app
#
from django import forms
from django.conf import settings
from django.forms.widgets import Textarea, Select, TextInput, HiddenInput
from lolyx.llx.models import Effectif, Sector, Typent
from lolyx.llx.models import RemoteWork
from .models import Company, ToolProposal
from crispy_forms.helper import FormHelper
from crispy_forms import layout
from crispy_forms.bootstrap import PrependedText, FormActions, InlineRadios


class CompanyNewForm(forms.ModelForm):
    """
    Use to create a new company
    """
    class Meta:
        model = Company
        fields = ["name", "description", "desc_mark", "adresse",
                  "cp", "ville", "contact_name", "contact_email",
                  "contact_tel", "effectif", "sector", "typent", "logo",
                  "lon", "lat"
                  ]

    name = forms.CharField(max_length=50,
                           required=True,
                           label="Nom",
                           widget=TextInput())

    description = forms.CharField(max_length=2500,
                                  required=False,
                                  widget=Textarea())

    desc_mark = forms.ChoiceField(required=True,
                                  label="Format",
                                  initial='1',
                                  choices=settings.MARKUP_CHOICES)

    adresse = forms.CharField(max_length=2500,
                              required=False,
                              widget=Textarea())

    cp = forms.CharField(max_length=2500,
                         required=False,
                         label="Code postal",
                         widget=TextInput())

    ville = forms.CharField(max_length=30,
                            required=False,
                            widget=TextInput())

    contact_name = forms.CharField(max_length=50,
                                   required=False,
                                   label="Personne à contacter",
                                   widget=TextInput())

    contact_email = forms.EmailField(max_length=50,
                                     required=False,
                                     widget=TextInput())

    contact_tel = forms.CharField(max_length=50,
                                  required=False,
                                  widget=TextInput())

    remotework = forms.ModelChoiceField(queryset=RemoteWork.objects.filter(id__gt=0),
                                        widget=Select(),
                                        label="Télétravail dans l'entreprise",
                                        required=True)

    effectif = forms.ModelChoiceField(queryset=Effectif.objects.filter(id__gt=0),
                                      widget=Select(),
                                      label="Effectif de l'entreprise",
                                      required=True)

    sector = forms.ModelChoiceField(queryset=Sector.objects.filter(id__gt=0).order_by('name'),
                                    widget=Select(),
                                    label="Secteur d'activité",
                                    required=True)

    typent = forms.ModelChoiceField(queryset=Typent.objects.filter(id__gt=0).order_by('name'),
                                    widget=Select(),
                                    label="Type d'entreprise",
                                    required=True)
    
    logo = forms.ImageField(label="Logo",
                            help_text="Logo de l'entreprise",
                            required=False)

    lon = forms.FloatField(required=True,
                           initial="0.0",
                           widget=HiddenInput())

    lat = forms.FloatField(required=True,
                           initial="0.0",
                           widget=HiddenInput())

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.field_class = 'col-lg-10'
    helper.layout = layout.Layout(
        'lon', 'lat',
        layout.Field('name', css_class='input-large'),
        layout.Field('description'),
        InlineRadios('desc_mark'),
        layout.Field('adresse', rows="2"),
        layout.Field('cp'),
        layout.Field('ville'),
        'effectif',
        'sector',
        'typent',
        'remotework',
        'logo',

        layout.Fieldset('Contact',
                        layout.Field('contact_name', css_class='input-large'),
                        layout.Field('contact_tel', css_class='input-small'),
                        PrependedText('contact_email', '@', css_class='input-xlarge'),
                        ),
        layout.Div(
            layout.HTML(u'''<div id="div_id_geoloc" class="form-group">
  <label for="id_geoloc" class="control-label col-lg-2">
    Géolocalisation
  </label>
  <div class="controls col-lg-10">
    <div class="input-group">
      <div id="companymap"></div>
    </div>
  </div>
</div>''')
            ),

        FormActions(
            layout.Submit('save_changes', 'Enregistrer', css_class="btn-primary"),
            layout.Submit('cancel', 'Annuler', css_class="btn-danger"),
            ))


class ToolProposalForm(forms.ModelForm):
    """A new tool
    """
    class Meta(object):
        model = ToolProposal
        # user will be forced in views
        exclude = ('user', 'category', 'family')

    name = forms.CharField(max_length=30,
                           required=True,
                           label="Nom",
                           widget=TextInput())

    slug = forms.CharField(max_length=30,
                           help_text="Nom court (doit être unique)")

    url = forms.CharField(max_length=300,
                          help_text="Homepage du projet pour un logiciel, page d'information générale sinon")

    comment = forms.CharField(max_length=300,
                              widget=Textarea(),
                              help_text="Un petit commentaire pour motiver votre demande")

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.field_class = 'col-lg-10'
    helper.layout = layout.Layout(
        layout.Field('name'),
        layout.Div(
            layout.HTML(u'''<div id="div_id_exists" class="form-group">
  <label for="id_exists" class="control-label col-lg-2"></label>
  <div class="controls col-lg-10">
    <div id="name_exists_container" class="input-group">Valeurs existantes : <span id="name_exists"></span></div>
  </div>
</div>''')),
        layout.Field('slug'),
        layout.Div(
            layout.HTML(u'''<div id="div_id_exists" class="form-group">
  <label for="id_exists" class="control-label col-lg-2"></label>
  <div class="controls col-lg-10">
    <div id="slug_exists_container" class="input-group">Valeurs existantes : <span id="slug_exists"></span></div>
  </div>
</div>''')),
        layout.Field('url'),
        layout.Field('comment'),
        FormActions(
            layout.Submit('save_changes', 'Enregistrer', css_class="btn-primary"),
            layout.Submit('cancel', 'Annuler', css_class="btn-danger"))
        )
