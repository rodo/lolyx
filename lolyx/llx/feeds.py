# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib.syndication.views import Feed
from lolyx.llx.models import Tool


class LatestToolFeed(Feed):
    title = "Lolix derniers outils"
    link = "/"
    description = "Les outils et compétences dans Lolix."

    def items(self):
        return Tool.objects.all().order_by('-pk')[:5]

    def item_title(self, item):
        return item.name

    def item_description(self, item):
        return item.url
