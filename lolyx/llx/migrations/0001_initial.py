# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('external', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name=b'Name')),
                ('description', models.CharField(max_length=3000, null=True, verbose_name=b'Description', blank=True)),
                ('description_markup', models.IntegerField(default=0, choices=[(0, b'text/plain'), (1, b'markdown'), (2, b'reST')])),
                ('last_login', models.DateTimeField(null=True, blank=True)),
                ('last_modification', models.DateTimeField(null=True, blank=True)),
                ('date_creation', models.DateTimeField(auto_now_add=True)),
                ('slug', models.SlugField(null=True, max_length=30, blank=True, unique=True, verbose_name=b'slug')),
                ('note', models.CharField(max_length=3000, null=True, verbose_name=b'Note intern', blank=True)),
                ('website', models.URLField(null=True, blank=True)),
                ('adh_april', models.BooleanField(default=False, verbose_name=b'Adherent april')),
                ('ssii', models.BooleanField(default=False)),
                ('cabrecrut', models.BooleanField(default=False)),
                ('ulule', models.BooleanField(default=False)),
                ('stay_alive', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('adresse', models.CharField(max_length=300, null=True, blank=True)),
                ('cp', models.CharField(max_length=10, null=True, blank=True)),
                ('ville', models.CharField(max_length=30, null=True, blank=True)),
                ('telephone', models.CharField(max_length=30, null=True, blank=True)),
                ('url', models.URLField(max_length=300, null=True, blank=True)),
                ('contact_name', models.CharField(max_length=50, null=True, verbose_name=b'Contact', blank=True)),
                ('contact_email', models.EmailField(max_length=100, null=True, verbose_name=b'Contact email', blank=True)),
                ('contact_tel', models.CharField(max_length=30, null=True, verbose_name=b'Contact tel', blank=True)),
                ('logo', models.ImageField(null=True, upload_to=b'logo/', blank=True)),
                ('nb_acc', models.PositiveSmallIntegerField(default=0)),
                ('nb_ref', models.PositiveSmallIntegerField(default=0)),
                ('lon', models.FloatField(default=0.0)),
                ('lat', models.FloatField(default=0.0)),
            ],
        ),
        migrations.CreateModel(
            name='CompanyRessource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('account', models.CharField(max_length=255)),
                ('company', models.ForeignKey(to='llx.Company')),
                ('ressource', models.ForeignKey(to='external.Ressource')),
            ],
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(max_length=30, verbose_name=b'Contract code')),
            ],
        ),
        migrations.CreateModel(
            name='Diplom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Effectif',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Faq',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('question', models.CharField(max_length=500)),
                ('answer', models.CharField(max_length=3000)),
                ('score', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=30)),
                ('user_from', models.ForeignKey(related_name='userfrom', to=settings.AUTH_USER_MODEL)),
                ('user_to', models.ForeignKey(related_name='userto', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Lang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('code', models.CharField(max_length=2, null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Motd',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=False)),
                ('home', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=300)),
                ('body', models.TextField(max_length=3000)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Poste',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('slug', models.SlugField(unique=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'Region name')),
                ('job_online', models.SmallIntegerField(null=True, blank=True)),
                ('res_online', models.SmallIntegerField(null=True, blank=True)),
                ('insee', models.IntegerField(null=True, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='RemoteWork',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=30, verbose_name=b'Intitul\xc3\xa9')),
            ],
        ),
        migrations.CreateModel(
            name='Sector',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
                ('slug', models.SlugField(unique=True, max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Tool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=30, verbose_name=b'Tool name')),
                ('slug', models.SlugField(unique=True, max_length=30)),
                ('url', models.URLField(max_length=300, blank=True)),
                ('category', models.PositiveSmallIntegerField(default=0)),
                ('family', models.PositiveIntegerField(default=0)),
                ('odds', models.PositiveIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ToolAssociated',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('odds', models.PositiveIntegerField(default=0)),
                ('tms', models.DateTimeField(auto_now_add=True)),
                ('one', models.ForeignKey(related_name='one', to='llx.Tool')),
                ('two', models.ForeignKey(related_name='two', to='llx.Tool')),
            ],
        ),
        migrations.CreateModel(
            name='ToolProposal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'Tool name')),
                ('slug', models.SlugField(unique=True, max_length=30)),
                ('url', models.URLField(max_length=300, blank=True)),
                ('category', models.PositiveSmallIntegerField(default=0)),
                ('family', models.PositiveIntegerField(default=0)),
                ('comment', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Typent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='UserExtra',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('extra', models.TextField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserPoint',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('points', models.PositiveIntegerField(default=0)),
                ('active', models.BooleanField(default=False)),
                ('date_updated', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('karma', models.SmallIntegerField(default=0)),
                ('status', models.PositiveSmallIntegerField(default=0, choices=[(0, b'unknow'), (1, b'candidate'), (2, b'recruiters')])),
                ('monthly_points', models.IntegerField(default=500)),
                ('jobmoder_allowed', models.BooleanField(default=False)),
                ('jobmoder_blacklist', models.BooleanField(default=False)),
                ('jobmoder_point', models.PositiveSmallIntegerField(default=1)),
                ('lon', models.FloatField(default=0.0)),
                ('lat', models.FloatField(default=0.0)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='company',
            name='effectif',
            field=models.ForeignKey(default=0, to='llx.Effectif'),
        ),
        migrations.AddField(
            model_name='company',
            name='remotework',
            field=models.ForeignKey(default=0, to='llx.RemoteWork'),
        ),
        migrations.AddField(
            model_name='company',
            name='sector',
            field=models.ForeignKey(default=0, to='llx.Sector'),
        ),
        migrations.AddField(
            model_name='company',
            name='typent',
            field=models.ForeignKey(default=0, to='llx.Typent'),
        ),
        migrations.AddField(
            model_name='company',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterUniqueTogether(
            name='toolassociated',
            unique_together=set([('one', 'two')]),
        ),
    ]
