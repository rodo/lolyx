# -*- coding: utf-8 -*-
#
# Copyright (c) 2014-2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf import settings
from django.db import connection
from django import template
from django.utils.html import conditional_escape
from django.template import Template, Context
from django.utils.safestring import mark_safe

register = template.Library()

@register.simple_tag
def sqlqry():
    if settings.DEBUG:
        time = 0.0
        for q in connection.queries:
            time += float(q['time'])
            
        t = Template('''<p><em>Total sql query count :</em> {{ count }}, 
        <em>Total sql execution time:</em> {{ time }} ms</p>
        ''')
                        
        return t.render(Context({'count':len(connection.queries),
                                 'time':time}))
    else:
        return ""
