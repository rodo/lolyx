# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
import urllib
from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.conf import settings
from django.http import HttpResponse
from lolyx.routing.utils import readjson


logger = logging.getLogger(__name__)


@cache_page(settings.LLXIO_TIMECACHE)
def llxio(request):
    """
    Call llxio url shortener
    """
    url = request.get_full_path()[12:]
    qurl = urllib.quote("http://lolix.org{}".format(urllib.unquote(url)))
    llxiourl = "http://llx.io/?url={}".format(qurl)
    data = readjson(llxiourl)
    return HttpResponse(data, mimetype='application/json')

