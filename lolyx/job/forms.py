# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.conf import settings
from django import forms
from django.forms.widgets import Textarea, TextInput, Select, CheckboxInput, HiddenInput, RadioSelect
from lolyx.llx.models import Company
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div, Submit, HTML, Button, Row, Field, Fieldset
from crispy_forms.bootstrap import AppendedText, PrependedText, FormActions, InlineRadios


class JobSearch(forms.Form):
    """
    Use to create Job
    """
    query = forms.CharField(max_length=50,
                            required=True,
                            label=_("Title"),
                            widget=TextInput())

class JobForm(forms.Form):
    """
    Use to create Job
    """
    title = forms.CharField(max_length=50,
                            required=True,
                            label=_("Title"),
                            widget=TextInput())

    family = forms.ChoiceField(required=True,
                               label="Type",
                               choices=((settings.FAMILY_EMPLOI, 'Emploi'),
                                        (settings.FAMILY_STAGE, 'Stage'),
                                        (settings.FAMILY_MISSION, 'Mission')),
                               widget = RadioSelect(),
                               initial = '0',
                               )

    poste_desc = forms.CharField(max_length=2500,
                                 label="Description du poste",
                                 required=False,
                                 widget=Textarea())

    desc_markup =   forms.ChoiceField(required=True,
                                      label=_("Format"),
                                      initial='1',
                                      choices=settings.MARKUP_CHOICES,
                                      widget = RadioSelect())


    contact_name = forms.CharField(max_length=50,
                                   label=_("Contact"),
                                   required=False,
                                   help_text="Nom de la personne à contacter pour ce poste",
                                   widget=TextInput())

    contact_email = forms.EmailField(max_length=50,
                                     required=True,
                                     label=_("Email"),
                                     help_text="Cet email n'est jamais communiqué à quiconque, nous y redirigeons les emails envoyés à l'adresse publique de l'offre")

    contact_tel = forms.CharField(max_length=50,
                                  label="Téléphone",
                                  required=False,
                                  widget=TextInput())

    salaire = forms.CharField(max_length=50,
                              label="Rémunération",
                              required=False,
                              widget=TextInput())

    ref = forms.CharField(max_length=50,
                          label="Référence",
                          help_text="Référence de cette offre au sein de votre entreprise",
                          required=False)

    creation = forms.BooleanField(required=False,
                                  label="Création de poste",
                                  widget=CheckboxInput())

    teletravail = forms.BooleanField(required=False,
                                     label="Télétravail possible, à négocier",
                                     widget=CheckboxInput())

    lon = forms.FloatField(required=True,
                           initial="0.0",
                           widget=HiddenInput())

    lat = forms.FloatField(required=True,
                           initial="0.0",
                           widget=HiddenInput())


class JobAuthForm(JobForm):
    """
    Use to create Job for authenticated users
    """
    company = forms.ModelChoiceField(queryset=Company.objects.all(),
                                     widget=Select(),
                                     label="Société",
                                     required=True)

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-lg-2'
    helper.field_class = 'col-lg-10'
    helper.layout = Layout(
        'lon','lat',
        Field('title', css_class='input-large'),
        Field('company'),
        InlineRadios('family'),
        Field('creation'),
        Field('teletravail', style="background: #FAFAFA; padding: 10px;"),
        Field('poste_desc', rows="8", css_class='input-large'),
        InlineRadios('desc_markup'),
        'salaire',
        'ref',
        Fieldset('<h4>Contact</h4>',
                 Field('contact_name', css_class='input-large'),
                 Field('contact_tel', css_class='input-small'),
                 PrependedText('contact_email', '@', css_class='input-xlarge'),
                 ),
        FormActions(
            Submit('save_changes', 'Etape 2', css_class="btn-primary"),
            Submit('cancel', 'Annuler', css_class="btn-danger"),
            )
        )
