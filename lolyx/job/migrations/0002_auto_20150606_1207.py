# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import djorm_pgfulltext.fields
import django.db.models.deletion
from django.conf import settings
import django.contrib.postgres.fields.hstore
import uuidfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),        
        ('llx', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('job', '0001_hstore'),
    ]

    operations = [
        migrations.CreateModel(
            name='Job',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('family', models.IntegerField(default=0, choices=[(0, b'emploi'), (1, b'stage'), (2, b'mission')])),
                ('title', models.CharField(max_length=300, verbose_name=b'Job title', blank=True)),
                ('ref', models.CharField(max_length=50, verbose_name=b'Reference', blank=True)),
                ('description', models.TextField(verbose_name=b'Description', blank=True)),
                ('description_markup', models.IntegerField(default=0, choices=[(0, b'text/plain'), (1, b'markdown'), (2, b'reST')])),
                ('poste', models.CharField(max_length=50, verbose_name=b'Poste', blank=True)),
                ('salaire', models.CharField(max_length=50, verbose_name=b'Salaire', blank=True)),
                ('creation', models.BooleanField(default=False, verbose_name=b'Cr\xc3\xa9ation de poste')),
                ('teletravail', models.BooleanField(default=False, verbose_name=b'Teletravail')),
                ('status', models.PositiveSmallIntegerField(default=0, choices=[(0, b'Editing'), (1, b'Online'), (2, b'Moderation'), (3, b'Refused'), (4, b'Desactive'), (5, b'Deleted')])),
                ('contact_name', models.CharField(max_length=50, verbose_name=b'Contact', blank=True)),
                ('contact_email', models.EmailField(max_length=100, verbose_name=b'Contact email', blank=True)),
                ('contact_tel', models.CharField(max_length=30, verbose_name=b'Contact tel', blank=True)),
                ('viewed', models.PositiveIntegerField(default=0)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_published', models.DateTimeField(auto_now_add=True)),
                ('date_expired', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('gnu', models.BooleanField(default=False)),
                ('moderation_score', models.SmallIntegerField(default=0)),
                ('moderation_votes', models.PositiveSmallIntegerField(default=0)),
                ('lon', models.FloatField(default=0.0)),
                ('lat', models.FloatField(default=0.0)),
                ('cost', models.SmallIntegerField(default=0)),
                ('search_index', djorm_pgfulltext.fields.VectorField(default=b'', serialize=False, null=True, editable=False, db_index=True)),
                ('author', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=django.db.models.deletion.PROTECT)),
                ('company', models.ForeignKey(to='llx.Company', on_delete=django.db.models.deletion.PROTECT)),
                ('fkposte', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='llx.Poste', null=True)),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='llx.Region', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='JobBookmark',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='JobLang',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.SmallIntegerField(default=3)),
                ('tms', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
                ('lang', models.ForeignKey(to='llx.Lang')),
            ],
        ),
        migrations.CreateModel(
            name='JobOnline',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('tools', django.contrib.postgres.fields.hstore.HStoreField(null=True, blank=True)),
                ('job', models.ForeignKey(to='job.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobRepublishKey',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', uuidfield.fields.UUIDField(unique=True, max_length=32, editable=False, blank=True)),
                ('generated', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobStat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
            ],
        ),
        migrations.CreateModel(
            name='JobTool',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.SmallIntegerField(default=3)),
                ('contrib', models.SmallIntegerField(default=0)),
                ('experience', models.SmallIntegerField(default=0)),
                ('tms', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
                ('tool', models.ForeignKey(to='llx.Tool', on_delete=django.db.models.deletion.PROTECT)),
            ],
        ),
        migrations.CreateModel(
            name='JobUserView',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('job', models.ForeignKey(to='job.Job')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='JobVote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('vote', models.SmallIntegerField()),
                ('job', models.ForeignKey(to='job.Job')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterIndexTogether(
            name='jobvote',
            index_together=set([('user', 'job')]),
        ),
        migrations.AlterIndexTogether(
            name='jobuserview',
            index_together=set([('user', 'datetime')]),
        ),
        migrations.AlterUniqueTogether(
            name='jobtool',
            unique_together=set([('job', 'tool')]),
        ),
        migrations.AlterIndexTogether(
            name='jobtool',
            index_together=set([('id', 'tool')]),
        ),
        migrations.AlterIndexTogether(
            name='jobrepublishkey',
            index_together=set([('job', 'key')]),
        ),
        migrations.AlterUniqueTogether(
            name='joblang',
            unique_together=set([('job', 'lang')]),
        ),
        migrations.AlterUniqueTogether(
            name='jobbookmark',
            unique_together=set([('user', 'job')]),
        ),
        migrations.AlterIndexTogether(
            name='jobbookmark',
            index_together=set([('user', 'date')]),
        ),
    ]
