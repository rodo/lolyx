# -*- coding: utf-8 -*-
#
# Copyright (c) 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
The job views
"""
import logging
import json
from django.conf import settings

from django.core.cache import cache
from django.views.decorators.cache import cache_page
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseForbidden, HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from lolyx.llx.models import Tool, Lang
from lolyx.job.models import Job, JobTool, JobStat, JobVote, JobBookmark, JobUserView
from django.contrib.auth.decorators import login_required


@login_required
def addtool(request, pk, toolid):
    """Add a tool to a job

    Called by typeahead to add a new Tool in a Job
    """
    job = get_object_or_404(Job, pk=pk)
    tool = get_object_or_404(Tool, pk=toolid)
    if request.user.id == job.company.user.id:
        try:
            rest = JobTool.objects.create(job=job, tool=tool)
            result = rest.id
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


@login_required
def lvltool(request, pk, toolid, level):
    """Change the a level's tool    

    Called by typeahead to change the level in a job
    """
    job = get_object_or_404(Job, pk=pk)
    rest = get_object_or_404(JobTool, pk=toolid)
    if request.user.id == job.company.user.id:
        try:
            rest.level = int(level)
            rest.save()
            result = rest.id
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


@login_required
def deltool(request, pk, jobtoolid):
    """Delete tool from a job

    Called by typeahead to add a new Tool in a Job
    """
    job = get_object_or_404(Job, pk=pk)
    tool = get_object_or_404(JobTool, pk=jobtoolid)
    if request.user.id == job.company.user.id:
        try:
            tool.delete()
            result = 1
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


@login_required
def addlang(request, pk, langid):
    """Add a lang to a job

    Called by typeahead to add a new Lang in a Job
    """
    job = get_object_or_404(Job, pk=pk)
    lang = get_object_or_404(Lang, pk=langid)
    if request.user.id == job.company.user.id:
        try:
            rest = JobLang.objects.create(job=job, lang=lang)
            result = rest.id
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404


@login_required
def dellang(request, pk, joblangid):
    """Delete lang from a job

    Called by typeahead to add a new Lang in a Job
    """
    job = get_object_or_404(Job, pk=pk)
    lang = get_object_or_404(JobLang, pk=joblangid)
    if request.user.id == job.company.user.id:
        try:
            lang.delete()
            result = 1
        except:
            result = 0
        return HttpResponse(json.dumps({'result': result}), mimetype='application/json')
    else:
        return Http404
