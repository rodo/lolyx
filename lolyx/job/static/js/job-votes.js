/*
 *
 *
 *
 *
 */

$(document).ready(
    function() {
	/*
	 * Add progress bar on job with moderation status
	 * 
	 */
	$.get("/offres/myjobs/",
	      function (data) {
		  var prog = '<div class="progress-bar progress-bar-{{color}}" role="progressbar" aria-valuenow="{{valuenow}}" aria-valuemin="0" aria-valuemax="{{valuemax}}" style="width: {{width}}%;">'
		  + '{{valuenow}}   </div>';

		  $.each(data,
			 function(index, object) {
			     $.each(object.moderation,
				    function(index, object) {

					var sumv = object.refuse + 5;

					if (object.accept > 0) {
					    $('#job-progress-'+object.id).append(
						Mustache.render(prog,
								{'valuenow': object.accept,
								 'valuemax': object.votes,
								 'width': object.accept / sumv * 100,
								 'color': 'success'}
							   ));
					};
					if (object.refuse > 0) {
					    $('#job-progress-'+object.id).append(
						Mustache.render(prog,
								{'valuenow': object.refuse,
								 'valuemax': object.votes,
								 'width': object.refuse / sumv * 100,
								 'color': 'danger'}
							   ));
					}

				    });
			 });

	      }, 'json');
    }
);
