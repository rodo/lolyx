
$(document).ready(
    /* Typehead initialisation
     * 
     */
    function() {
	var button = '<div class="btn-group job-tool" id="job-tool-{{id}}">'
	    + '<button id="job-tool-btn-{{id}}" type="button" class="btn btn-sm btn-primary">{{name}}</button>'
	    + '<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown">'
	    + '<span class="caret"></span>'
	    + '<span class="sr-only">Toggle Dropdown</span>'
	    + '</button>'
	    + '  <ul class="dropdown-menu" role="menu">'
	    + '  <li>'
	    + '<a href="#" onclick="leveltool({{id}}, 1)";>Expert '
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '</a>'
	    + '</li>'
	    + '<li><a href="#" onclick="leveltool({{id}}, 2)";>Bien maitrisé '
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '</a>'
	    + '</li>'
	    + '<li><a href="#" onclick="leveltool({{id}}, 2)";>Maitrisé '
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '</a>'
	    + '</li>'
	    + '<li><a href="#" onclick="leveltool({{id}}, 3)";>Bonnes connaissances '
	    + '<span class="glyphicon glyphicon-star"></span>'
	    + '</a>'
	    + '</li>'
	    + '<li><a href="#" onclick="leveltool({{id}}, 3)";>Connu'
	    + '</a>'
	    + '</li>'
	    + '<li class="divider"></li>'
	    + '<li><a href="#" onclick="deltool({{id}});">Supprimer</a></li>'
	    + '</ul></div>';

	$.get('/ressources/typeahead/t/',
	      function(data){
		  $("#tool-typeahead").typeahead(
		      {
			  source: function(query, process) {

			      objects = [];
			      map = {
			      };
			      $.each(data, function(i, object) {
					 map[object.label] = object;
					 objects.push(object.label);
				     });
			      process(objects);
			  },
			  updater: function(item) {
			      var id = 0;
			      $.each(data, function(i, object) {
					 if (item == object.label) {
					     id = object.id;
					 }
				     });
			      if (id > 0) {
				  var urladd = 'addtool/' + id;
				  $.get(urladd,
					function (data) {
					    /* if the tool is correctly added
					     * result is > 0
					     */
					    if (data.result > 0) {
						var span = Mustache.render(button,
									   {name: item,
									   id: data.result});
						$('#job-tools').append(span);
						
					    }
					    $('#tool-typeahead').val('');
					}, 'json');
			      }
			      return item;
			  }


		      }
		  );
	      },'json');
    }
);