#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Import old database
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from lolyx.llx.models import Company, UserProfile
from lolyx.job.models import Job, JobTool
from lolyx.job.models import JobLang
from lolyx.job.models import JobOnline
from lolyx.job.models import JobRepublishKey
import MySQLdb


def decenc(data):
    if data:
        return data.decode('iso-8859-1').encode('utf8')
    else:
        return None

def notnullstr(data):
    if data is None:
        return ''
    else:
        return data

class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.db = self.mysqlconnect()
        Job.objects.all().delete()
        Company.objects.all().delete()

        self.readdatas()

    def mysqlconnect(self):
        db = MySQLdb.connect(host="localhost", db="lolix")
        return db

    def readdatas(self):
        # you must create a Cursor object. It will let
        # you execute all the queries you need
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT l.login, s.* FROM societe s",
                          "INNER JOIN login as l ON s.id = l.id",
                          "ORDER BY s.id DESC",
                          "LIMIT %s"])

        limit = 300

        cur.execute(query, (limit, ))
        i = 0
        for row in cur.fetchall() :
            i = i + 1
            print i, row['idp'], row['login']
            self.create_company(row)

        print query

    def create_company(self, data):

        login = decenc(data["login"])
        user = None

        try:
            user, create = User.objects.get_or_create(username=login)

            userp = UserProfile.objects.get(user=user)
            userp.status = settings.USER_PROFILE_COMPANY
            userp.save()
        except:
            print login

        if (user):
            oldid = data["idp"]
            name = notnullstr(decenc(data['nom']))

            newr = Company(id=oldid,
                           user=user,
                           name=name,
                           date_creation=data['datea'],
                           description=decenc(data['description']),
                           adresse=decenc(data['address']),
                           cp=decenc(data['cp']),
                           ville=decenc(data['ville']),                           
                           url="http://%s" % (decenc(data['url'])),
                           slug=decenc(data['alias'])
                           )

            newr.last_login=data['datel']
            newr.last_modification=data['datem']

            newr.save()
