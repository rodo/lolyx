#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Import old database
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from lolyx.job.models import Job, JobTool
from lolyx.job.models import JobLang
from lolyx.job.models import JobOnline
from lolyx.job.models import JobRepublishKey
from lolyx.llx.models import Tool, Company, Lang
import MySQLdb

def decenc(data):
    if data:
        return data.decode('iso-8859-1').encode('utf8')
    else:
        return None

def notnullstr(data):
    if data is None:
        return ''
    else:
        return data

class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.db = self.mysqlconnect()
        Job.objects.all().delete()
        JobOnline.objects.all().delete()        
        JobTool.objects.all().delete()
        JobRepublishKey.objects.all().delete()

        self.readdatas()

    def mysqlconnect(self):
        db = MySQLdb.connect(host="localhost", db="lolix")
        return db

    def tools(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM outiloffre",
                          "WHERE fk_offre=%s"])

        cur.execute(query, (id, ))
        return cur.fetchall()

    def langs(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM langoffre",
                          "WHERE fk_offre=%s"])

        cur.execute(query, (id, ))
        return cur.fetchall()

    def readdatas(self):
        # you must create a Cursor object. It will let
        # you execute all the queries you need
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT *",
                          "FROM offre",
                          "ORDER BY id DESC",
                          "LIMIT %s"])

        limit = 600

        cur.execute(query, (limit, ))
        i = 0
        for row in cur.fetchall() :
            print row['idp'], row['fk_soc']
            self.create_job(row)

        print query

    def get_status(self, status):

        if status == 1:
            return 1
        else:
            return 0

    def create_job(self, data):

        try:
            company = Company.objects.get(pk=data['fk_soc'])
        except:
            company = None

        if company is not None:
            oldid = data["idp"]

            status = self.get_status(data["active"])

            newjob = Job(id=oldid,
                         author_id=company.user.id,
                         company=company,
                         status=status,
                         description_markup=0,
                         viewed=data['viewed'],
                         date_created=data['datec'],
                         title=notnullstr(decenc(data['titre'])))

            if data['ref'] is not None:
                newjob.ref = decenc(data['ref'])

            if data['dated'] is not None:
                date_expired = data['dated']

            if decenc(data['details']) is not None:
                newjob.description = decenc(data['details'])

            if decenc(data['salaire']) is not None:
                newjob.salaire = decenc(data['salaire'])

            if decenc(data['c_nom']) is not None or decenc(data['c_prenom']) is not None:
                newjob.contact_name = "%s %s" % (decenc(data['c_nom']),
                                                 decenc(data['c_prenom']))

            if decenc(data['c_mail']) is not None:
                newjob.contact_email = decenc(data['c_mail'])

            if decenc(data['c_tel']) is not None:
                newjob.contact_tel = decenc(data['c_tel'])

            # 3 - stage
            # 4 - freelance
            if data['fk_contrat'] == 3:
                newjob.family = settings.FAMILY_STAGE
            if data['fk_contrat'] == 4:
                newjob.family = settings.FAMILY_MISSION

            newjob.save()
            #
            # Import des outils
            #
            tools = []
            for tool in self.tools(oldid):
                tools.append(JobTool(job=newjob,
                                     level=tool['fk_niveau'],
                                     tool=Tool.objects.get(pk=tool['fk_outil'])))
            JobTool.objects.bulk_create(tools)
            #
            # Import des langues
            #
            langs = []
            for lang in self.langs(oldid):
                langs.append(JobLang(job=newjob,
                                     level=lang['fk_niv'],
                                     lang=Lang.objects.get(pk=lang['fk_lang'])))
            JobLang.objects.bulk_create(langs)
            #
            #
            #
            tool_list = {}
            tools = JobTool.objects.filter(job_id=newjob.id).values_list('tool__slug', 'tool__name')
            for tool in tools:
                tool_list[tool[0]] = tool[1]
                JobOnline.objects.create(job=newjob,
                                         tools=tool_list)
            
