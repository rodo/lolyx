#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Import old database
"""
from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from lolyx.job.models import Job, JobTool, JobPhp
from lolyx.llx.models import Tool, Company
import MySQLdb

def decenc(data):
    if data:
        return data.decode('iso-8859-1').encode('utf8')
    else:
        return None

def notnullstr(data):
    if data is None:
        return ''
    else:
        return data

class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.db = self.mysqlconnect()
        Job.objects.all().delete()
        JobTool.objects.all().delete()
        JobPhp.objects.all().delete()

        self.readdatas()

    def mysqlconnect(self):
        db = MySQLdb.connect(host="localhost", db="lolix")
        return db

    def tools(self, id):
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT * FROM outiloffre",
                          "WHERE fk_offre=%s"])

        cur.execute(query, (id, ))
        return cur.fetchall()

    def readdatas(self):
        # you must create a Cursor object. It will let
        # you execute all the queries you need
        cur = self.db.cursor(MySQLdb.cursors.DictCursor)

        query = " ".join(["SELECT *",
                          "FROM offre",
                          "ORDER BY id DESC",
                          "LIMIT %s"])

        limit = 600

        cur.execute(query, (limit, ))
        i = 0
        for row in cur.fetchall() :
            i = i + 1
            print i, row['idp'], row['fk_soc']
            self.create_job(row)

        print query

    def get_status(self, status):

        if status == 1:
            return 1
        else:
            return 0

    def create_job(self, data):

        try:
            company = Company.objects.get(pk=data['fk_soc'])
        except:
            company = None

        if company is not None:
            oldid = data["idp"]

            status = self.get_status(data["active"])

            newjob = Job(author_id=company.user.id,
                         company=company,
                         status=status,
                         description_markup=0,
                         title=notnullstr(decenc(data['titre'])))

            if data['ref'] is not None:
                newjob.ref = decenc(data['ref'])

            if decenc(data['details']) is not None:
                newjob.description = decenc(data['details'])

            if data['salaire'] is not None:
                newjob.salaire = decenc(data['salaire'])

            newjob.save()

            JobPhp.objects.create(job=newjob,
                                  oldid=oldid)


            tools = []
            for tool in self.tools(oldid):
                tools.append(JobTool(job=newjob,
                                     level=tool['fk_niveau'],
                                     tool=Tool.objects.get(pk=tool['fk_outil'])))
            JobTool.objects.bulk_create(tools)
