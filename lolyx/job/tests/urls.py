# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Test all public and private urls
"""
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User, Group
from django.test import TestCase, Client
from lolyx.llx.models import Company, UserProfile
from lolyx.job.models import Job


class UrlsTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        User.objects.all().delete()
        Group.objects.all().delete()
        Job.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')

        self.company = Company.objects.create(user=self.user,
                                              name='Foo corp')

    def test_single_job(self):
        """
        Create a simple job
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Senior admin',
                                 status=1)

        client = Client()
        response = client.get('/offres/{}/'.format(job.id))

        self.assertContains(response, 'Senior admin', status_code=200)

    def test_home(self):
        """
        The home
        """
        client = Client()
        response = client.get('/offres/')

        self.assertContains(response, 'Lolix', status_code=200)

    def test_feed_latest(self):
        """
        The home
        """
        Job.objects.create(author=self.user,
                           company=self.company,
                           title='Senior admin',
                           status=1)

        client = Client()
        response = client.get('/offres/feed/latest/')
        self.assertContains(response, 'Senior', status_code=200)

    def test_feed_latest_hidden(self):
        """
        Feed
        """
        Job.objects.create(author=self.user,
                           company=self.company,
                           title='Hidden',
                           status=settings.JOB_STATUS_EDITION)

        client = Client()
        response = client.get('/offres/feed/latest/')

        self.assertNotContains(response, 'Hidden', status_code=200)

    def test_moderator_auth(self):
        """
        The moderator view

        - the user is a moderator, is allowed
        """
        username = 'modo_auth'
        password = 'password'
        modo = User.objects.create_user(username,   'admin_search@bar.com',          password)

        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        modo.groups.add(group)

        Job.objects.create(author=self.user,
                           company=self.company,
                           title='Hidden',
                           status=settings.JOB_STATUS_MODERATION)

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(reverse('jobs_moderators'))

        self.assertEqual(response.status_code, 200)

    def test_moderator_unauth(self):
        """
        The moderator view

        - the user is _not_ a moderator, a 404 is raised
        """
        username = 'modo_unauth'
        password = 'password'
        User.objects.create_user(username,
                                 'admin_search@bar.com',
                                 password)

        Job.objects.create(author=self.user,
                           company=self.company,
                           title='Hidden',
                           status=settings.JOB_STATUS_MODERATION)

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(reverse('jobs_moderators'))

        self.assertEqual(response.status_code, 403)

    def test_unpublish_unauth(self):
        """Unpublish a job with a non authenticated user

        - the user is not authenticated
        """
        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_PUBLISHED)

        url = "{}unpublish/".format(job.get_absolute_url())

        client = Client()
        response = client.get(url)

        job = Job.objects.get(pk=job.id)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(job.status, settings.JOB_STATUS_PUBLISHED)

    def test_unpublish_notowner(self):
        """Unpublish a job, user is not the owner

        Result : a 404 error is raised
        """
        username = 'user_notowner'
        password = 'password'
        User.objects.create_user(username,
                                 'admin_search@bar.com',
                                 password)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_PUBLISHED)

        url = "{}unpublish/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        job = Job.objects.get(pk=job.id)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(job.status, settings.JOB_STATUS_PUBLISHED)

    def test_unpublish_owner(self):
        """Unpublish a job, user is the owner

        Result : the action is done
        """
        username = 'user_owner'
        password = 'password'
        owner = User.objects.create_user(username,
                                         'admin_search@bar.com',
                                         password)

        job = Job.objects.create(author=owner,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_PUBLISHED)

        url = "{}unpublish/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        job = Job.objects.get(pk=job.id)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(job.status, settings.JOB_STATUS_ARCHIVED)

    def test_unpublish_notonline(self):
        """Unpublish a job, user is the owner, the job is not online

        Result : the action is done
        """
        username = 'user_owner'
        password = 'password'
        owner = User.objects.create_user(username,
                                         'admin_search@bar.com',
                                         password)

        job = Job.objects.create(author=owner,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_EDITION)

        url = "{}unpublish/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        job = Job.objects.get(pk=job.id)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(job.status, settings.JOB_STATUS_EDITION)

    def test_unpublish_moderator(self):
        """Unpublish a job, user is a moderator

        Result : the action is done
        """
        username = 'user_owner'
        password = 'password'
        modo = User.objects.create_user(username,
                                        'admin_search@bar.com',
                                        password)

        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        modo.groups.add(group)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_PUBLISHED)

        url = "{}unpublish/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        job = Job.objects.get(pk=job.id)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(job.status, settings.JOB_STATUS_ARCHIVED)


    def test_archives(self):
        """The archive view
        """
        username = 'user_archives'
        password = 'password'
        modo = User.objects.create_user(username,
                                        'admin_search@bar.com',
                                        password)

        # job not owned
        Job.objects.create(author=self.user,
                           company=self.company,
                           title='BarfooKo',
                           status=settings.JOB_STATUS_ARCHIVED)

        # job owned
        Job.objects.create(author=modo,
                           company=self.company,
                           title='FoobarOk',
                           status=settings.JOB_STATUS_ARCHIVED)

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(reverse('jobs_archives'))

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'FoobarOk')
        self.assertNotContains(response, 'BarfooKo')

    def test_job_moderate(self):
        """The moderate view of a job
        """
        username = 'user_jobmoderate'
        password = 'password'
        modo = User.objects.create_user(username,
                                        'admin_search@bar.com',
                                        password)

        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        modo.groups.add(group)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_MODERATION)

        url = "{}moderate/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 200)

    def test_job_moderate_nonmodo(self):
        """The moderate view of a job

        The user is not a moderator, a 403 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        User.objects.create_user(username, 'admin_search@bar.com', password)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_MODERATION)

        url = "{}moderate/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)

    def test_refuse(self):
        """The refuse view by a regular user

        - user : is NOT allowed in userprofil
        - job : in right status

        - 403 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        User.objects.create_user(username, 'admin_search@bar.com', password)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_MODERATION)

        url = "{}refuse/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)

    def test_refuse_allowed(self):
        """The refuse view by an allowed user

        - user : is allowed in userprofil
        - job : in right status

        - 302 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        user = User.objects.create_user(username, 'foo@bar.com', password)
        prof = UserProfile.objects.get(user=user)
        prof.jobmoder_allowed = True
        prof.save()

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_MODERATION)

        url = "{}refuse/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)

    def test_refuse_allowed_wrongstatus(self):
        """The refuse view by an allowed user

        - user : is allowed in userprofil
        - job : in WRONG status

        - 403 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        user = User.objects.create_user(username, 'foo@bar.com', password)
        prof = UserProfile.objects.get(user=user)
        prof.jobmoder_allowed = True
        prof.save()

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_EDITION)

        url = "{}refuse/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)

    def test_refuse_moderator(self):
        """The refuse view by an allowed user

        - user : is a moderator
        - job : in right status

        - 302 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        modo = User.objects.create_user(username, 'foo@bar.com', password)
        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        modo.groups.add(group)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_MODERATION)

        url = "{}refuse/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 302)

    def test_refuse_moderator_wrongstatus(self):
        """The refuse view by an allowed user

        - user : is a moderator
        - job : in WRONG status

        - 403 is raised
        """
        username = 'user_nomodo'
        password = 'password'

        modo = User.objects.create_user(username, 'foo@bar.com', password)
        group = Group.objects.create(name=settings.GROUP_MODERATOR)
        modo.groups.add(group)

        job = Job.objects.create(author=self.user,
                                 company=self.company,
                                 title='Hidden',
                                 status=settings.JOB_STATUS_EDITION)

        url = "{}refuse/".format(job.get_absolute_url())

        client = Client()
        auth = client.login(username=username, password=password)
        response = client.get(url)

        self.assertTrue(auth)
        self.assertEqual(response.status_code, 403)
