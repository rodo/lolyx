# -*- coding: utf-8 -*-
#
# Copyright (c) 2013, 2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Unit tests for lolyx.job management commands
"""
from datetime import datetime, timedelta
from StringIO import StringIO
from django.conf import settings
from django.core.management import call_command
from django.test import TestCase
from django.contrib.auth.models import User
from lolyx.job.models import Job
from lolyx.llx.models import Company


class CommandTests(TestCase):
    """
    Test the management commands in usermgt
    """
    def setUp(self):
        """
        Init
        """
        Job.objects.all().delete()
        self.user = User.objects.create_user('foobar',
                                             'admin_search@bar.com',
                                             'admintest')
        self.company = Company.objects.create(user=self.user, name='Foo corp')


    def test_job_expire(self):
        """The cron command that expired the jobs

        One off the job job must be unpublished the other one no
        """
        job_exp = Job.objects.create(author=self.user,
                                     company=self.company,
                                     title='Senior admin',
                                     status=settings.JOB_STATUS_PUBLISHED,
                                     date_expired=datetime.now() - timedelta(hours=1))

        job_notexp = Job.objects.create(author=self.user,
                                        company=self.company,
                                        title='Senior admin',
                                        status=settings.JOB_STATUS_PUBLISHED,
                                        date_expired=datetime.now() + timedelta(hours=1))

        content = StringIO()
        call_command('job_expire', stdout=content)
        content.seek(0)

        jby = Job.objects.get(pk=job_exp.id)
        jbn = Job.objects.get(pk=job_notexp.id)

        self.assertEqual(jby.status, settings.JOB_STATUS_ARCHIVED)
        self.assertEqual(jbn.status, settings.JOB_STATUS_PUBLISHED)

