# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Models definition for external ressources
"""
from datetime import datetime
from django.db import models
from django.contrib.auth.models import User


class Ressource(models.Model):
    """
    The ressource project

    """
    name = models.CharField(max_length=300,
                            verbose_name='Name')

    status = models.IntegerField(default=0)

    classname = models.CharField(max_length=300,
                                 blank=True,
                                 verbose_name='Class name')

    url = models.URLField(max_length=300)
    userpage = models.URLField(max_length=300)

    api_url = models.URLField(max_length=300,
                              blank=True)
    # is the api must be query
    api_active = models.BooleanField(default=False)
    # Is the api call need a special user different than userid
    need_user_api = models.BooleanField(default=False)

    # Is in use
    active = models.BooleanField(default=False)

    # updated by management script external_munin
    # 
    nb_resume = models.PositiveSmallIntegerField(default=0)


    def __str__(self):
        return self.name


class UserRessource(models.Model):
    """
    A ressource associated to one user
    """
    epoch = datetime(1970, 1, 1, 0, 0, 0, 0)

    user = models.ForeignKey(User)
    ressource = models.ForeignKey(Ressource)
    # The user/userid/login that identified the user on external ressource
    userid = models.CharField(max_length=300)
    # Special user to use with API if different than userid
    user_api = models.CharField(max_length=300, blank=True)
    # auto fields
    last_checked = models.DateField(default=epoch)
    last_status = models.IntegerField(default=0)

    api_last_checked = models.DateField(default=epoch)
    api_last_status = models.IntegerField(default=0)

    def userpage(self):
        """Url of the user page on the external ressource
        """
        return self.ressource.userpage.format(self.userid)

    def api_url(self):
        """Return the full url to fetch api
        """
        if len(self.user_api):
            url = self.ressource.api_url.format(self.user_api)
        else:
            url = self.ressource.api_url.format(self.userid)

        return url


class UserRessourceArchive(models.Model):
    """
    Archive values read for futures usage
    """
    ures = models.ForeignKey(UserRessource)
    data = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
