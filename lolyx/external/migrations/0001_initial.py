# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),        
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Ressource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name=b'Name')),
                ('status', models.IntegerField(default=0)),
                ('classname', models.CharField(max_length=300, verbose_name=b'Class name', blank=True)),
                ('url', models.URLField(max_length=300)),
                ('userpage', models.URLField(max_length=300)),
                ('api_url', models.URLField(max_length=300, blank=True)),
                ('api_active', models.BooleanField(default=False)),
                ('need_user_api', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=False)),
                ('nb_resume', models.PositiveSmallIntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='UserRessource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('userid', models.CharField(max_length=300)),
                ('user_api', models.CharField(max_length=300, blank=True)),
                ('last_checked', models.DateField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('last_status', models.IntegerField(default=0)),
                ('api_last_checked', models.DateField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('api_last_status', models.IntegerField(default=0)),
                ('ressource', models.ForeignKey(to='external.Ressource')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserRessourceArchive',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('ures', models.ForeignKey(to='external.UserRessource')),
            ],
        ),
    ]
