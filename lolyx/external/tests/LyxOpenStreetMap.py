# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
OpenStreetMap plugin
"""
from django.test import TestCase
from lolyx.external.tests.httpserver import TestServer
from lolyx.external.utils.plugins.LyxOpenStreetMap import LyxOpenStreetMap


class LyxOpenStreetMapTests(TestCase):
    """
    The urls
    """
    def setUp(self):
        """
        set up the tests
        """
        self.datas = """<?xml version="1.0" encoding="UTF-8"?>
<osm version="0.6" generator="OpenStreetMap server">
  <user id="26348"      display_name="Rodolphe Quiédeville" 
     account_created="2008-02-04T12:40:59Z">
    <description>Lorem</description>
    <contributor-terms agreed="true"/>
    <img href="http://www.gravatar.com/avatar/95.jpg"/>
    <roles>
    </roles>
    <changesets count="1069"/>
    <traces count="14"/>
    <blocks>
      <received count="0" active="0"/>
    </blocks>
  </user>
</osm>"""

    def test_fetch(self):
        """
        Test the fetch function on a right url
        """
        http = TestServer()
        http.start()
        url = 'http://127.0.0.1:%d/fetch' % (http.port)

        plugin = LyxOpenStreetMap()
        (status, datas) = plugin.fetch(url)

        self.assertEqual(status, 200)
        self.assertEqual(datas, {'changesets': 0, 'traces': 0, 'type': 'osm'})

    def test_fetch_error404(self):
        """
        Test the fetch function on an invalid URL that returns a 404
        """
        http = TestServer()
        http.start()
        url = 'http://127.0.0.1:%d/error-404' % (http.port)

        plugin = LyxOpenStreetMap()
        (status, datas) = plugin.fetch(url)

        self.assertEqual(status, 404)
        self.assertEqual(datas, {})

    def test_parse(self):
        """
        Successful parse
        """
        plugin = LyxOpenStreetMap()

        response = plugin.parse(self.datas)
        attend = {'traces': 14, 'type': 'osm', 'changesets': 1069}
        self.assertEqual(response, attend)

    def test_parse_failed(self):
        """
        Unsuccessful parse
        """
        plugin = LyxOpenStreetMap()

        response = plugin.parse("")
        attend = {'traces': 0, 'type': 'osm', 'changesets': 0}
        self.assertEqual(response, attend)
