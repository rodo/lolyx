# -*- coding: utf-8 -*-
#
# Copyright (c) 2013 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
External plugins
"""
import requests
from django.conf import settings


class LyxExternalPlugin(object):

    try:
        uag = settings.USER_AGENT
    except:
        # TODO log an error
        uag = "user-agent not set"
    
    headers = {'User-Agent': uag}

    @classmethod
    def download(self, url):
        """Check the nodes
        """
        content = None

        req = requests.get(url, headers=self.headers)

        if req.status_code == 200:
            content = req.content

        return (req.status_code, content)

    @classmethod
    def head(self, url):
        """Check the url
        """
        try:
            req = requests.head(url, headers=self.headers)
            status = req.status_code
        except:
            status = 500

        return status
