# -*- coding: utf-8 -*-
#
# Copyright (c) 2013,2014 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from django.contrib import admin
from lolyx.external.models import Ressource, UserRessource


class RessourceAdmin(admin.ModelAdmin):
    """
    Custom Admin for Ressource
    """
    list_display = ('name', 'classname', 'api_active')
    list_filter = ('api_active',)
    ordering = ['name']


class UserRessourceAdmin(admin.ModelAdmin):
    """
    Custom Admin for UserRessource
    """
    list_display = ('user', 'ressource', 'last_status')
    list_filter = ('last_status', 'api_last_status')


admin.site.register(Ressource, RessourceAdmin)
admin.site.register(UserRessource, UserRessourceAdmin)
