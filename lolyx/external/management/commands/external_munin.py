#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Output stats for munin
"""
from django.core.cache import cache
from django.core.management.base import BaseCommand
from django.db.models import Count, F
from lolyx.external.models import Ressource
from lolyx.external.models import UserRessource


class Command(BaseCommand):
    help = 'Print stats in munin format'

    def handle(self, *args, **options):
        """
        Handle the munin command
        """
        self.count()
        self.update_ressource()

    def count(self):

        nb = UserRessource.objects.all().count()

        self.stdout.write("userressource.value %s\n" % (nb))

    def update_ressource(self):

        ressources = UserRessource.objects.values('ressource').order_by().annotate(Count('ressource'))
        for res in ressources:
            Ressource.objects.filter(pk=res['ressource']).update(nb_resume=res['ressource__count'])

