Lolyx
=====

Lolyx is a full rewrite of lolix project, lolix is the software run on
http://fr.lolix.org

AUTHORS
=======

 * Rodolphe Quiedeville <rodolphe@quiedeville.org>

Documentation
=============

* http://doc.lolix.org/

Developpers
===========

Init an instance
================

```console
$ rm /tmp/lolyx.sqlite  ; ./manage.py syncdb --noinput; ./manage.py loaddata lolyx/llx/fixtures/dev_data.json
```


i18n
----

```console
$ ./manage.py  makemessages -a -i compressor -i faker -i leaflet -i sphinx -i IPython
```

Please open issues on gitlab

Require
=======

See requirements.txt

Debian
------

 * python-requests
 * python-django
 * python-django-celery
