============
Installation
============

Création de la base de données
------------------------------

.. code-block:: sh

    $ manage.py makemigrations
    $ manage.py migrate sites
    $ manage.py migrate auth
    $ manage.py migrate
